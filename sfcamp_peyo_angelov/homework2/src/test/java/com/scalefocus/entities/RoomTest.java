package com.scalefocus.entities;

import com.scalefocus.enums.LuxurePackage;
import com.scalefocus.enums.RoomType;
import com.scalefocus.exceptions.InvalidPackageTypeException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class RoomTest {


    @BeforeClass
    public static void setupDate() {
        Date arivalDate = new Date(10 / 10 / 2019);
        Date leavingDate = new Date(11 / 11 / 2019);
    }

    @Mock
    Hotel hotel;
    //Room room;
    Reservation reservation;
    List<Reservation> reservationsList;

    @Test
    public void isRoomFreeForGivenPeriodTest() {
        Room room = Mockito.mock(Room.class);
        assertFalse(room.isRoomFreeForGivenPeriod(new Date(10 / 10 / 2010), new Date(12 / 12 / 2019)));
    }

    @Test
    public void isRoomFreeForGivenPeriodTestIsNotFree() throws InvalidPackageTypeException {
        Room room = Mockito.mock(Room.class);
        assertFalse(room.isRoomFreeForGivenPeriod(new Date(12 / 12 / 2019), new Date(10 / 10 / 2010)));
    }

    @Test(expected = InvalidPackageTypeException.class)
    public void isRoomFreeForGivenPeriodTestLeavingDateIsBeforeArrivalDate() throws InvalidPackageTypeException {
        // packageID is incorrect
        Room room = new Room(RoomType.SINGLE_ROOM, LuxurePackage.ORDINARY, 10, 1, BigDecimal.TEN, false, false, false, false, false, false, false);
    }

    @Test
    public void addNewReservationTest() {
        Room room = Mockito.mock(Room.class);
        Reservation reservation = Mockito.mock(Reservation.class);

        assertFalse("Reservation is done", room.addNewReservation(reservation));
    }

    @Test
    public void removeGivenReservation() {
        Room room = Mockito.mock(Room.class);
        Reservation reservation = Mockito.mock(Reservation.class);
        assertFalse("Reservation wasn't delete", room.removeGivenReservation(reservation));

    }
}