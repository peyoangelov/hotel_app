package com.scalefocus.entities;

import org.junit.Test;
import org.mockito.Mock;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class HotelTest {

 Hotel hotel = new Hotel();
   private final static Set<Room> rooms = new HashSet<>();
  @Mock
   Room room;

    @Test
    public void getInstanceTestIsNull() {
     assertTrue("The instance is null" ,Hotel.getInstance()!=null);
    }

   @Test
   public void getInstanceTestInstanceIsNotOne() {
      assertTrue("The instance is not only one" ,Hotel.getInstance().equals(Hotel.getInstance()));

   }
    @Test
    public void addRoomTest() {
       rooms.add(room);
       assertFalse("Collection is null", rooms.isEmpty());
    }

    @Test
    public void getAllRoomsTest() {
       assertTrue(rooms.containsAll(rooms));
    }
}