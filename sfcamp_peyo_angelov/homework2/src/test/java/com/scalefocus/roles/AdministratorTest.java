package com.scalefocus.roles;

import com.scalefocus.constants.HotelConstants;
import com.scalefocus.entities.Hotel;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;

import static org.junit.Assert.assertNotEquals;

public class AdministratorTest {

    @Test
    public void changePriceTest() {
        Administrator administrator = new Administrator();
        Hotel hotel = Mockito.mock(Hotel.class);
        BigDecimal actualPriceBeforeChange = HotelConstants.RoomConsts.VIP_APARTMENT_PRICE;
        BigDecimal newPrice = new BigDecimal(20);
        administrator.changePrice(hotel, 5, newPrice);
        assertNotEquals(actualPriceBeforeChange, newPrice);
        System.out.println(actualPriceBeforeChange + " " + newPrice);
    }

}