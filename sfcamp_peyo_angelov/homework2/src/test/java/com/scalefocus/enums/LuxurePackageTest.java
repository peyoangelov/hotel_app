package com.scalefocus.enums;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LuxurePackageTest {

    @Test
    public void getNameTestCorectEnumValueOrdinary() {
        String luxuryValue= "Ordinary";
        Assert.assertEquals(luxuryValue,LuxurePackage.ORDINARY.getName());
    }
    @Test
    public void getNameTestWrongEnumValueOrdinary() {
        String luxuryValue= "Ordinaryaaa";
        Assert.assertNotEquals((luxuryValue),LuxurePackage.ORDINARY.getName());
    }

    @Test
    public void getNameTestCorectEnumValueDeluxe() {
        String luxuryValue= "Deluxe";
        Assert.assertEquals(luxuryValue,LuxurePackage.DELUX.getName());
    }
    @Test
    public void getNameTestWrongEnumValueDelux() {
        String luxuryValue= "delXXXXXXXXXXXX";
        Assert.assertNotEquals((luxuryValue),LuxurePackage.DELUX.getName());
    }
    @Test
    public void getNameTestCorectEnumValueVip() {
        String luxuryValue= "VIP";
        Assert.assertEquals(luxuryValue,LuxurePackage.VIP.getName());
    }
    @Test
    public void getNameTestWrongEnumValueVIP() {
        String luxuryValue= "VVVVVVVVVvvvvvvvvi";
        Assert.assertNotEquals((luxuryValue),LuxurePackage.VIP.getName());
    }

    @Test
    public void getNameTestCorectEnumName() {
        String luxuryName= "ORDINARY";
        Assert.assertEquals(luxuryName,LuxurePackage.ORDINARY.name());
    }
    @Test
    public void getNameTestWrongEnumName() {
        String luxuryName= "orddinnnn";
        Assert.assertNotEquals((luxuryName),LuxurePackage.ORDINARY.name());
    }
}