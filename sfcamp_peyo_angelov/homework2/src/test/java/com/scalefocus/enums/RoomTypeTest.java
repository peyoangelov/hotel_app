package com.scalefocus.enums;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class RoomTypeTest {

    @Test
    public void getNameTestCorectEnumValueSingleRoom() {
        String luxuryValue= "Single room";
        Assert.assertEquals(luxuryValue,RoomType.SINGLE_ROOM.getName());
    }
    @Test
    public void getNameTestWrongEnumValueSingleRoom() {
        String luxuryValue= "Single roomMMMMMMM";
        Assert.assertNotEquals((luxuryValue),RoomType.SINGLE_ROOM.getName());
    }

    @Test
    public void getNameTestCorectEnumValueDoubleRoom() {
        String luxuryValue= "Double room";
        Assert.assertEquals(luxuryValue,RoomType.DOUBLE_ROOM.getName());
    }
    @Test
    public void getNameTestWrongEnumValueDoubleRoom() {
        String luxuryValue= "double roomMMMMMMM";
        Assert.assertNotEquals((luxuryValue),RoomType.DOUBLE_ROOM.getName());
    }

    @Test
    public void getNameTestCorectEnumValueAppartment() {
        String luxuryValue= "Apartment";
        Assert.assertEquals(luxuryValue,RoomType.APARTMENT.getName());
    }
    @Test
    public void getNameTestWrongEnumValueApartment() {
        String luxuryValue= "Apart roomMMMMMMM";
        Assert.assertNotEquals((luxuryValue),RoomType.APARTMENT.getName());
    }

    @Test
    public void getNameTestCorectEnumName() {
        String luxuryName= "APARTMENT";
        Assert.assertEquals(luxuryName,RoomType.APARTMENT.name());
    }
    @Test
    public void getNameTestWrongEnumName() {
        String luxuryName= "appp";
        Assert.assertNotEquals((luxuryName),RoomType.APARTMENT.name());
    }
}