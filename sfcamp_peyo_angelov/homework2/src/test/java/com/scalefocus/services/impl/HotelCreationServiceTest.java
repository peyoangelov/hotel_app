package com.scalefocus.services.impl;

import com.scalefocus.constants.HotelConstants;
import com.scalefocus.entities.Hotel;
import com.scalefocus.entities.Room;
import com.scalefocus.enums.RoomType;
import com.scalefocus.enums.UserRole;
import com.scalefocus.roles.Guest;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Date;

import static org.junit.Assert.*;

public class HotelCreationServiceTest {

    @Test
    public void createOrdinaryRoom() {
        Hotel hotel = Mockito.mock(Hotel.class);
        Guest guest = Mockito.mock(Guest.class);
        HotelCreationService hotelCreationService= new HotelCreationService();
        hotelCreationService.createOrdinaryRoom(RoomType.SINGLE_ROOM,1);
        guest.showListOfFreeRooms(hotel, UserRole.ORDINARY_USER,false,new Date(10 / 10 / 2010), new Date(10 / 10 / 2019));

    }

    @Test
    public void createDeluxeRoom() {
        Hotel hotel = Mockito.mock(Hotel.class);
        Guest guest = Mockito.mock(Guest.class);
        HotelCreationService hotelCreationService= new HotelCreationService();
        hotelCreationService.createOrdinaryRoom(RoomType.DOUBLE_ROOM,2);
        guest.showListOfFreeRooms(hotel, UserRole.ORDINARY_USER,false,new Date(10 / 10 / 2010), new Date(10 / 10 / 2019));
    }

    @Test
    public void createVipApartment() {
        Hotel hotel = Mockito.mock(Hotel.class);
        Guest guest = Mockito.mock(Guest.class);
        HotelCreationService hotelCreationService= new HotelCreationService();
        hotelCreationService.createOrdinaryRoom(RoomType.APARTMENT,3);
        guest.showListOfFreeRooms(hotel, UserRole.ORDINARY_USER,false,new Date(10 / 10 / 2010), new Date(10 / 10 / 2019));
    }

    @Test
    public void fillTheHotelWithRooms() {
        Hotel hotel = Mockito.mock(Hotel.class);
        Guest guest = Mockito.mock(Guest.class);
        Room room = Mockito.mock(Room.class);
        HotelCreationService hotelCreationService= new HotelCreationService();
        hotelCreationService.fillTheHotelWithRooms();
    }
}