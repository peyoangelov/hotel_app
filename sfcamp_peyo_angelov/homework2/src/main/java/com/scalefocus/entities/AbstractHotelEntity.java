package com.scalefocus.entities;

import com.scalefocus.constants.HotelConstants;
import com.scalefocus.exceptions.InvalidPackageTypeException;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractHotelEntity {
    private final Locale locale = new Locale("en", "EN");
    private final ResourceBundle resource = ResourceBundle.getBundle("locale", locale);
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractHotelEntity.class);

    private int entityId;
    private int packageId;
    private BigDecimal price;

    public AbstractHotelEntity(int entityId, int packageId) throws InvalidPackageTypeException {
        this.setEntityId(entityId);
        this.setPackageId(packageId);
    }

    public AbstractHotelEntity(int entityId, int packageId, BigDecimal price) throws InvalidPackageTypeException {
        this.setEntityId(entityId);
        this.setPackageId(packageId);
        this.setPrice(price);
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) throws InvalidPackageTypeException {
        if (packageId >= HotelConstants.RoomConsts.ORDINARY_SINGLE_ROOM_CODE && packageId <= HotelConstants.RoomConsts.VIP_APARTMENT_CODE) {
            this.packageId = packageId;
        } else {
            throw new InvalidPackageTypeException(resource.getString("message.invalid.package.type"));
        }
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        if (price.compareTo(BigDecimal.ZERO) > 0) {
            this.price = price;
        }
    }
}
