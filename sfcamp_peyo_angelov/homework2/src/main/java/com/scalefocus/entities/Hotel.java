package com.scalefocus.entities;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Hotel {


    private final static Set<Room> rooms = new HashSet<>();
    private static Hotel SINGLE_HOTEL_INSTANCE = null;

    /**
     * static method to create instance of Singleton class
     */
    public static Hotel getInstance() {
        if (SINGLE_HOTEL_INSTANCE == null)
            SINGLE_HOTEL_INSTANCE = new Hotel();

        return SINGLE_HOTEL_INSTANCE;
    }

    /**
     * Adding a room to the collection
     */
    public void addRoom(final Room newRoom) {
        if (newRoom != null)
            rooms.add(newRoom);
    }

    /**
     * Return the collection of all rooms, without change it
     *
     * @return
     */
    public Set<Room> getAllRooms() {
        return Collections.unmodifiableSet(rooms);
    }
}

