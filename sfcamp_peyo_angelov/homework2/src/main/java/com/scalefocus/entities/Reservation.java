package com.scalefocus.entities;

import com.scalefocus.exceptions.InvalidPackageTypeException;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.scalefocus.constants.HotelConstants.Date.DATE_FORMAT;

public class Reservation extends AbstractHotelEntity {

    private final static SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

    private static int reservationIdGenerator = 1;
    private Date arrivalDate;
    private Date leaveDate;
    private String guestName;

    /**
     * Constructor for a reservation
     *
     * @param packageId
     * @param arrivalDate
     * @param leaveDate
     */
    public Reservation(int packageId, String guestName, Date arrivalDate, Date leaveDate) throws InvalidPackageTypeException {
        super(reservationIdGenerator, packageId);
        this.setArrivalDate(arrivalDate);
        this.setLeaveDate(leaveDate);
        this.setGuestName(guestName);
        reservationIdGenerator++;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(final Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(final Date leaveDate) {
        this.leaveDate = leaveDate;
    }

    /**
     * Make better Date format
     *
     * @param date
     * @return better date format
     */
    private String beautifyDate(final Date date) {
        return sdf.format(date);
    }

    public String getFormattedArrivalDate() {
        return beautifyDate(this.getArrivalDate());
    }

    public String getFormattedLeaveDate() {
        return beautifyDate(this.getLeaveDate());
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }
}
