package com.scalefocus.entities;

import com.scalefocus.enums.LuxurePackage;
import com.scalefocus.enums.RoomType;
import com.scalefocus.exceptions.InvalidPackageTypeException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Room extends AbstractHotelEntity {
    private RoomType roomType;
    private LuxurePackage luxurePackage;
    private boolean hasWifi;
    private boolean hasCleanEveryDay;
    private boolean hasChangeTowelsEveryDay;
    private boolean hasBreakfast;
    private boolean hasFreeRoomService;
    private boolean hasFreeMassage;
    private boolean hasKingSizeBed;
    private List<Reservation> reservationsList;

    /**
     * Constructor for creating a room
     *
     * @param roomType
     * @param luxurePackage
     * @param packageId
     * @param roomNumber
     * @param price
     * @param hasWifi
     * @param hasCleanEveryDay
     * @param hasChangeTowelsEveryDay
     * @param hasBreakfast
     * @param hasFreeRoomService
     * @param hasFreeMassage
     * @param hasKingSizeBed
     */
    public Room(RoomType roomType, LuxurePackage luxurePackage, int packageId, int roomNumber, BigDecimal price, boolean hasWifi,
                boolean hasCleanEveryDay, boolean hasChangeTowelsEveryDay, boolean hasBreakfast, boolean hasFreeRoomService,
                boolean hasFreeMassage, boolean hasKingSizeBed) throws InvalidPackageTypeException {
        super(roomNumber, packageId, price);
        this.setRoomType(roomType);
        this.setLuxurePackage(luxurePackage);
        this.setHasWifi(hasWifi);
        this.setHasBreakfast(hasBreakfast);
        this.setHasChangeTowelsEveryDay(hasChangeTowelsEveryDay);
        this.setHasCleanEveryDay(hasCleanEveryDay);
        this.setHasFreeRoomService(hasFreeRoomService);
        this.setHasFreeMassage(hasFreeMassage);
        this.setHasKingSizeBed(hasKingSizeBed);
        this.reservationsList = new ArrayList<>();
    }

    /**
     * Check if the room is free for a specific period of time
     *
     * @param arrivingDate
     * @param leavingDate
     * @return
     */
    public boolean isRoomFreeForGivenPeriod(final Date arrivingDate, final Date leavingDate) {
        boolean startFlag = true;
        boolean endFlag = true;
        for (Reservation res : this.reservationsList) {
            if (res.getLeaveDate().before(arrivingDate)) {
                startFlag = false;
            }
            if (res.getArrivalDate().after(leavingDate)) {
                endFlag = false;
            }
            if (!startFlag || !endFlag) {
                break;
            }
        }
        return startFlag && endFlag;
    }

    public boolean addNewReservation(final Reservation r) {
        this.reservationsList.add(r);
        return true;
    }

    public boolean removeGivenReservation(final Reservation r) {
        return this.reservationsList.remove(r);
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }


    public LuxurePackage getLuxurePackage() {
        return luxurePackage;
    }

    public void setLuxurePackage(LuxurePackage luxurePackage) {
        this.luxurePackage = luxurePackage;
    }

    public boolean isHasWifi() {
        return hasWifi;
    }

    public void setHasWifi(boolean hasWifi) {
        this.hasWifi = hasWifi;
    }

    public boolean isHasCleanEveryDay() {
        return hasCleanEveryDay;
    }

    public void setHasCleanEveryDay(boolean hasCleanEveryDay) {
        this.hasCleanEveryDay = hasCleanEveryDay;
    }

    public boolean isHasChangeTowelsEveryDay() {
        return hasChangeTowelsEveryDay;
    }

    public void setHasChangeTowelsEveryDay(boolean hasChangeTowelsEveryDay) {
        this.hasChangeTowelsEveryDay = hasChangeTowelsEveryDay;
    }

    public boolean isHasBreakfast() {
        return hasBreakfast;
    }

    public void setHasBreakfast(boolean hasBreakfast) {
        this.hasBreakfast = hasBreakfast;
    }

    public boolean isHasFreeRoomService() {
        return hasFreeRoomService;
    }

    public void setHasFreeRoomService(boolean hasFreeRoomService) {
        this.hasFreeRoomService = hasFreeRoomService;
    }

    public boolean isHasFreeMassage() {
        return hasFreeMassage;
    }

    public void setHasFreeMassage(boolean hasFreeMassage) {
        this.hasFreeMassage = hasFreeMassage;
    }

    public boolean isHasKingSizeBed() {
        return hasKingSizeBed;
    }

    public void setHasKingSizeBed(boolean hasKingSizeBed) {
        this.hasKingSizeBed = hasKingSizeBed;
    }

    public List<Reservation> getReservationsList() {
        return reservationsList;
    }
}
