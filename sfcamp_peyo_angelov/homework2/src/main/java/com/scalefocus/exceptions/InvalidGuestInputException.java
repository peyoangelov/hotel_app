package com.scalefocus.exceptions;

public class InvalidGuestInputException extends Exception {
    private static final String lineSeparator = System.getProperty("line.separator");

    public InvalidGuestInputException(String message) {
        super(message + lineSeparator);
    }
}
