package com.scalefocus.exceptions;

public class InvalidPackageTypeException extends Exception {

    private static final String lineSeparator = System.getProperty("line.separator");

    public InvalidPackageTypeException(String message) {
        super(message + lineSeparator);
    }

}
