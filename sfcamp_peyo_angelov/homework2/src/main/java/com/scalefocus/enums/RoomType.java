package com.scalefocus.enums;

/**
 * Enum class with different room types
 */
public enum RoomType {

    SINGLE_ROOM("Single room"), DOUBLE_ROOM("Double room"), APARTMENT("Apartment");

    private String name;

    private RoomType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
