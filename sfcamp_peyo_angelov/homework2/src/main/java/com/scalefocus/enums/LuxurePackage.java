package com.scalefocus.enums;

/**
 * Enum class with different Luxury packages
 */
public enum LuxurePackage {

    ORDINARY("Ordinary"), DELUX("Deluxe"), VIP("VIP");

    String name;

    private LuxurePackage(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
