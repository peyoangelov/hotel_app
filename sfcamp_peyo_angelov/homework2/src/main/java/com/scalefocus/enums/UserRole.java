package com.scalefocus.enums;

public enum UserRole {
    ADMIN, ORDINARY_USER
}
