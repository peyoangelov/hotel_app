package com.scalefocus.roles;


import com.scalefocus.entities.Hotel;
import com.scalefocus.entities.Room;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

public class Administrator extends FrontDesc {
    private final Locale locale = new Locale("en", "EN");
    private final ResourceBundle resource = ResourceBundle.getBundle("locale", locale);
    private static final Logger LOGGER = LoggerFactory.getLogger(Administrator.class);

    /**
     * Admin can change price of a luxury package
     *
     * @param hotel
     * @param packageId
     * @param newPrice
     */
    public void changePrice(final Hotel hotel, int packageId, final BigDecimal newPrice) {
        final Set<Room> changedPriceRooms = hotel.getAllRooms().stream().filter(r -> r.getPackageId() == packageId).collect(Collectors.toSet());

        for (Room room : changedPriceRooms) {
            room.setPrice(newPrice);

        }
        LOGGER.info(resource.getString("message.price.was.changed.successfully") + getLineSeparator());
    }

}
