package com.scalefocus.roles;


import com.scalefocus.entities.Reservation;
import com.scalefocus.exceptions.InvalidPackageTypeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class Guest extends AbstractRole {
    private final Locale locale = new Locale("en", "EN");
    private final ResourceBundle resource = ResourceBundle.getBundle("locale", locale);
    private static final Logger LOGGER = LoggerFactory.getLogger(Guest.class);

    /**
     * @param guestName
     * @param arrival
     * @param daysToStay
     * @param packageId
     * @throws InvalidPackageTypeException
     */
    public void requestRoom(final String guestName, final Date arrival, final int daysToStay, int packageId) throws InvalidPackageTypeException {

        Date leaving = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(leaving);
        c.add(Calendar.DATE, daysToStay);
        leaving = c.getTime();
        final Reservation reservation = new Reservation(packageId, guestName, arrival, leaving);

        super.addPendingReservationRequest(reservation);
        LOGGER.info(MessageFormat.format(resource.getString("message.your.reservation.id"), reservation.getEntityId()) + getLineSeparator());
    }
}
