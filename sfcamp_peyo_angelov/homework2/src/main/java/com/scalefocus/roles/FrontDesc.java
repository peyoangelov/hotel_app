package com.scalefocus.roles;


import com.scalefocus.entities.Hotel;
import com.scalefocus.entities.Reservation;
import com.scalefocus.entities.Room;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

public class FrontDesc extends AbstractRole {
    private static final Logger LOGGER = LoggerFactory.getLogger(FrontDesc.class);
    private final Locale locale = new Locale("en", "EN");
    private final ResourceBundle resource = ResourceBundle.getBundle("locale", locale);

    /**
     * Show all pending reservations from guests
     */
    public boolean showPendingReservations() {
        if (getGuestsRequest().isEmpty()) {
            return false;
        }
        for (Reservation guestRequest : getGuestsRequest()) {
            LOGGER.info(MessageFormat.format(resource.getString("message.reservation.list"), guestRequest.getGuestName(), guestRequest.getEntityId(), guestRequest.getFormattedArrivalDate(), guestRequest.getFormattedLeaveDate()) + getLineSeparator());
        }
        return true;
    }

    /**
     * Book a room with specific reservation ID
     *
     * @param hotel
     * @param reservationId
     */
    public void makeBooking(Hotel hotel, int reservationId) {

        Reservation reservation = null;
        for (Reservation g : getGuestsRequest()) {
            if (g.getEntityId() == reservationId) {
                reservation = g;
                break;
            }
        }
        final Reservation pendingReservation = reservation;

        final Optional<Room> reservedRoom = hotel.getAllRooms().stream()
                .filter(r -> (r.isRoomFreeForGivenPeriod(pendingReservation.getArrivalDate(), pendingReservation.getLeaveDate()) == true && r.getPackageId() == pendingReservation.getPackageId()))
                .findFirst();

        if (reservedRoom.isPresent()) {
            Room room = reservedRoom.get();
            room.addNewReservation(pendingReservation);
            approveGuestResevation(reservation);
            LOGGER.info(resource.getString("message.reservation.approved") + getLineSeparator());
        }
    }

    /**
     * Approve a reservation by removing it from the guest Requests
     *
     * @param g
     */
    private void approveGuestResevation(final Reservation g) {
        getGuestsRequest().remove(g);
    }


}


