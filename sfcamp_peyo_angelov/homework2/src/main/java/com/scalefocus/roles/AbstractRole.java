package com.scalefocus.roles;


import com.scalefocus.entities.AbstractHotelEntity;
import com.scalefocus.entities.Hotel;
import com.scalefocus.entities.Reservation;
import com.scalefocus.entities.Room;
import com.scalefocus.enums.UserRole;
import com.scalefocus.services.impl.ReportGenerator;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractRole {
    private final Locale locale = new Locale("en", "EN");
    private final ResourceBundle resource = ResourceBundle.getBundle("locale", locale);
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRole.class);
    private static final String lineSeparator = System.getProperty("line.separator");

    private static Set<Reservation> guestsRequest;


    final static ReportGenerator reportGenerator;

    static {
        guestsRequest = new HashSet<>();
        reportGenerator = new ReportGenerator();
    }

    /**
     * Show List with all free rooms
     *
     * @param hotel
     * @param showPrice
     * @param arrivingDate
     * @param leavingDate
     */
    public void showListOfFreeRooms(final Hotel hotel, final UserRole role, final boolean showPrice, final Date arrivingDate, final Date leavingDate) {
        if (role.equals(UserRole.ADMIN)) {
            reportGenerator.generateReportForFreeRoomsInJSON(hotel, arrivingDate, leavingDate);
        } else {
            reportGenerator.generateReportForFreeRoomsInHTML(hotel, arrivingDate, leavingDate);
        }
    }

    public static Set<Reservation> getGuestsRequest() {
        return guestsRequest;
    }

    /**
     * Adding guest request
     */

    public static void addPendingReservationRequest(final Reservation r) {
        guestsRequest.add(r);
    }

    /**
     * Cancel the reservation
     */

    public boolean cancelReservation(final Hotel hotel, int reservationId) {
        // first check if reservation is part of pending reservations
        for (Reservation g : getGuestsRequest()) {
            if (g.getEntityId() == reservationId) {
                LOGGER.info(resource.getString("message.reservation.canceled") + lineSeparator);
                return getGuestsRequest().remove(g);
            }
        }

        // if function is not terminated above,
        // search in already approved reservations
        final Set<Room> hotelRooms = hotel.getAllRooms();
        for (Room r : hotelRooms) {
            if (r.getReservationsList() != null) {
                Reservation resToRemove = null;
                for (Reservation rs : r.getReservationsList()) {
                    if (rs.getEntityId() == reservationId) {
                        resToRemove = rs;
                        break;
                    }
                }
                if (resToRemove != null) {
                    LOGGER.info(resource.getString("message.reservation.canceled") + lineSeparator);
                    return r.removeGivenReservation(resToRemove);
                }
            }
        }
        LOGGER.info(resource.getString("message.reservation.not.exist") + lineSeparator);
        return false;
    }

    public static String getLineSeparator() {
        return lineSeparator;
    }
}
