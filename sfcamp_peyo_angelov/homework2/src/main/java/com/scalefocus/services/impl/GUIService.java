package com.scalefocus.services.impl;

import com.scalefocus.entities.Hotel;
import com.scalefocus.enums.LuxurePackage;
import com.scalefocus.enums.RoomType;
import com.scalefocus.enums.UserRole;
import com.scalefocus.exceptions.InvalidGuestInputException;
import com.scalefocus.exceptions.InvalidPackageTypeException;
import com.scalefocus.roles.AbstractRole;
import com.scalefocus.roles.Administrator;
import com.scalefocus.roles.FrontDesc;
import com.scalefocus.roles.Guest;
import com.scalefocus.services.GUIServiceInterface;
import com.scalefocus.services.HotelAdministrationServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import static com.scalefocus.constants.HotelConstants.Date.DATE_FORMAT;
import static com.scalefocus.constants.HotelConstants.Date.DATE_REGEX;
import static com.scalefocus.constants.HotelConstants.Methods.*;
import static com.scalefocus.constants.HotelConstants.Timer.TIMER_INTERVAL;

public class GUIService implements GUIServiceInterface {
    private final Locale locale = new Locale("en", "EN");
    private final ResourceBundle resource = ResourceBundle.getBundle("locale", locale);
    private static final Logger LOGGER = LoggerFactory.getLogger(GUIService.class);
    private final static SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
    private static final String lineSeparator = System.getProperty("line.separator");

    final static Guest guest = new Guest();
    final static FrontDesc frontDesc = new FrontDesc();
    final static Administrator administrator = new Administrator();
    final static ReportGenerator reportGenerator = new ReportGenerator();
    final static HotelCreationService hotelCreationService = new HotelCreationService();

    private Date arrivalDate;
    private Date leavingDate;

    private static String inputForRole;
    private static String inputForMethod;
    private static int enteredRoleId;
    private static int enteredMethodId;


    /**
     * Print information about occupied rooms on every 30 secs
     */
    @Override
    public void makeReportEvery30s(final Hotel hotel, final HotelAdministrationServiceInterface hotelAdministrationService, final Timer timer) {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                hotelAdministrationService.collectInformationAboutOccupiedRooms(hotel);
            }
        }, TIMER_INTERVAL, TIMER_INTERVAL);
    }

    /**
     * Take input int to know the exact role
     */
    @Override
    public void inputForRoleMethod(final Scanner sc, final Hotel hotel) {
        LOGGER.info(resource.getString("message.select.role") + lineSeparator);

        inputForRole = sc.next();
        enteredRoleId = validateIntUserInput(inputForRole);
        while (enteredRoleId == -1 && (enteredRoleId < 1 || enteredRoleId > 3)) {
            LOGGER.info(resource.getString("message.select.role.invalid") + lineSeparator);
            inputForRole = sc.next();
            enteredRoleId = validateIntUserInput(inputForRole);
        }


        switch (enteredRoleId) {
            case 1:
                inputForDifferentMethodsForGuest(sc, hotel);

                break;
            case 2:
                inputForDifferentMethodsForFrontDesc(sc, hotel);

                break;
            case 3:
                inputForDifferentMethodsForAdmin(sc, hotel);

                break;
        }
    }

    /**
     * Take input int to know the exact method for role GUEST
     */
    private void inputForDifferentMethodsForGuest(final Scanner sc, final Hotel hotel) {
        LOGGER.info(resource.getString("message.select.options.guest") + lineSeparator);
        inputForMethod = sc.next();
        enteredMethodId = validateIntUserInput(inputForMethod);

        while (enteredMethodId == -1 && isInvalidMethodInput(MAX_VALUE_FOR_METHOD_FOR_GUEST, MIN_VALUE_FOR_METHOD_FOR_GUEST)) {
            LOGGER.info(resource.getString("message.select.options.guest.invalid") + lineSeparator);
            inputForMethod = sc.next();
            enteredMethodId = validateIntUserInput(inputForMethod);
        }
        switch (enteredMethodId) {
            case 1:
                handleDateInputs(sc);
                LOGGER.info(MessageFormat.format(resource.getString("message.show.free.rooms"), arrivalDate, leavingDate));
                guest.showListOfFreeRooms(hotel, UserRole.ORDINARY_USER, true, arrivalDate, leavingDate);

                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;
            case 2:
                LOGGER.info(resource.getString("message.enter.user.name"));
                String guestName = sc.next();
                LOGGER.info(resource.getString("message.enter.room.type") + lineSeparator);

                int inputForPackageId = sc.nextInt();
                LOGGER.info(resource.getString("message.enter.arrival.date"));
                String arrivalDateInput = sc.next();
                while (!isDateInputValidFormat(arrivalDateInput)) {
                    LOGGER.info(resource.getString("message.enter.arrival.date.invalid"));
                    arrivalDateInput = sc.next();
                }
                LOGGER.info(resource.getString("message.enter.days.to.stay"));
                int daysToStay = sc.nextInt();
                try {
                    validateGuestInputReservation(guestName, inputForPackageId, arrivalDateInput, daysToStay);
                    isDateInputValidFormat(arrivalDateInput);
                    arrivalDate = sdf.parse(arrivalDateInput);

                    guest.requestRoom(guestName, arrivalDate, daysToStay, inputForPackageId);
                    LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                    inputForRoleMethod(sc, hotel);
                } catch (ParseException e) {
                    LOGGER.info(resource.getString("message.enter.date.invalid.format") + lineSeparator);
                } catch (InvalidGuestInputException | InvalidPackageTypeException e) {
                    LOGGER.info(e.getMessage());
                } finally {
                    inputForDifferentMethodsForGuest(sc, hotel);
                    break;
                }
            case 3:
                LOGGER.info(resource.getString("message.enter.reservation.id") + lineSeparator);
                int inputForReservationId = sc.nextInt();
                guest.cancelReservation(hotel, inputForReservationId);

                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;
            case 4:
                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;
        }
    }

    /**
     * Take input int to know the exact method for role Front DESC
     */
    private void inputForDifferentMethodsForFrontDesc(final Scanner sc, final Hotel hotel) {
        LOGGER.info(resource.getString("message.select.options.frontdesc") + lineSeparator);
        inputForMethod = sc.next();
        enteredMethodId = validateIntUserInput(inputForMethod);

        while (enteredMethodId == -1 && isInvalidMethodInput(MAX_VALUE_FOR_METHOD_FOR_FRONT_DESC, MIN_VALUE_FOR_METHOD_FOR_FRONT_DESC)) {
            LOGGER.info(resource.getString("message.select.options.frontdesc.invalid") + lineSeparator);
            inputForMethod = sc.next();
            enteredMethodId = validateIntUserInput(inputForMethod);

        }

        switch (enteredMethodId) {
            case 1:
                int inputForReservationId;
                LOGGER.info(resource.getString("message.show.pending.reservations") + lineSeparator);
                frontDesc.showPendingReservations();
                try {
                    LOGGER.info(resource.getString("message.to.approve.enter.reservation.id") + lineSeparator);
                    inputForReservationId = sc.nextInt();
                    frontDesc.makeBooking(hotel, inputForReservationId);
                } catch (final NullPointerException exception) {
                    LOGGER.info(resource.getString("message.no.pending.reservations"));
                }

                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;
            case 2:
                LOGGER.info(resource.getString("message.to.cancel.reservation.enter.id") + lineSeparator);
                inputForReservationId = sc.nextInt();
                frontDesc.cancelReservation(hotel, inputForReservationId);
                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;
            case 3:

                handleDateInputs(sc);
                LOGGER.info(MessageFormat.format(resource.getString("message.show.free.rooms"), arrivalDate, leavingDate) + lineSeparator);
                frontDesc.showListOfFreeRooms(hotel, UserRole.ORDINARY_USER, false, arrivalDate, leavingDate);
                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;
            case 4:
                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;
        }
    }

    /**
     * Take input int to know the exact method for role ADMIN
     */
    private void inputForDifferentMethodsForAdmin(final Scanner sc, final Hotel hotel) {
        LOGGER.info(resource.getString("message.select.option.admin") + lineSeparator);


        inputForMethod = sc.next();
        enteredMethodId = validateIntUserInput(inputForMethod);

        while (enteredMethodId == -1 && isInvalidMethodInput(MAX_VALUE_FOR_METHOD_FOR_ADMIN, MIN_VALUE_FOR_METHOD_FOR_ADMIN)) {
            LOGGER.info(resource.getString("message.select.option.admin.invalid") + lineSeparator);
            inputForMethod = sc.next();
            enteredMethodId = validateIntUserInput(inputForMethod);

        }

        switch (enteredMethodId) {
            case 1:
                handleDateInputs(sc);
                LOGGER.info(MessageFormat.format(resource.getString("message.show.free.rooms.for.admin.json"), arrivalDate, leavingDate));
                administrator.showListOfFreeRooms(hotel, UserRole.ADMIN, true, arrivalDate, leavingDate);
                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;
            case 2:
                LOGGER.info(resource.getString("message.to.cancel.reservation.enter.id"));
                int inputForReservationId = sc.nextInt();
                administrator.cancelReservation(hotel, inputForReservationId);
                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;
            case 3:
                LOGGER.info(resource.getString("message.option.luxury.package.change.price") + lineSeparator);
                int packageId = sc.nextInt();
                LOGGER.info(resource.getString("message.enter.new.price"));
                BigDecimal newPrice = sc.nextBigDecimal();
                administrator.changePrice(hotel, packageId, newPrice);
                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;

            case 4:
                LOGGER.info(resource.getString("message.json.extract.for.taken.rooms") + lineSeparator);
                reportGenerator.generateReportForTakenRoomsInJSON(hotel);
                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;

            case 5:
                handleDateInputs(sc);
                LOGGER.info(resource.getString("message.json.extract.for.free.rooms") + lineSeparator);
                reportGenerator.generateReportForFreeRoomsInJSON(hotel, arrivalDate, leavingDate);
                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;
            case 6:
                LOGGER.info(resource.getString("message.return.to.main.menu") + lineSeparator);
                inputForRoleMethod(sc, hotel);
                break;
        }
    }

    private boolean isInvalidMethodInput(int maxValue, int minValue) {

        return enteredMethodId < minValue || enteredMethodId > maxValue;
    }


    private void validateGuestInputReservation(final String guestName, int inputForPackageId, String arrivalDateInput, int daysToStay) throws InvalidGuestInputException, InvalidPackageTypeException {
        if (guestName == null || guestName.isEmpty()) {
            throw new InvalidGuestInputException(resource.getString("message.invalid.name.input"));
        } else if (inputForPackageId <= 0 || inputForPackageId > 5) {
            throw new InvalidPackageTypeException(resource.getString("message.enter.room.type.invalid") + lineSeparator);
        } else if (arrivalDateInput == null) {
            throw new InvalidGuestInputException(resource.getString("message.enter.date.invalid.format"));
        } else if (daysToStay < 1) {
            throw new InvalidGuestInputException(resource.getString("message.enter.days.to.stay.invalid"));
        }
    }

    private boolean isDateInputValidFormat(final String input) {
        return Pattern.matches(DATE_REGEX, input);
    }

    private void handleDateInputs(final Scanner sc) {
        LOGGER.info(resource.getString("message.enter.arrival.date"));
        String guestArrivalDate = sc.next();
        while (!isDateInputValidFormat(guestArrivalDate)) {
            LOGGER.info(resource.getString("message.enter.arrival.date.invalid"));
            guestArrivalDate = sc.next();
        }
        LOGGER.info(resource.getString("message.enter.leave.date"));
        String guestLeavingDate = sc.next();
        while (!isDateInputValidFormat(guestLeavingDate)) {
            LOGGER.info(resource.getString("message.enter.leaving.date.invalid"));
            guestLeavingDate = sc.next();
        }
        try {
            arrivalDate = sdf.parse(guestArrivalDate);
            leavingDate = sdf.parse(guestLeavingDate);
        } catch (ParseException e) {
            e.getMessage();
        }
    }

    public void createHotelRooms(final Hotel hotel, final Scanner sc) {
        LOGGER.info(resource.getString("message.select.create.option"));
        inputForMethod = sc.next();
        enteredMethodId = validateIntUserInput(inputForMethod);

        while (enteredMethodId == -1 && isInvalidMethodInput(2, 1)) {
            LOGGER.info(resource.getString("message.select.create.option") + lineSeparator);
            inputForMethod = sc.next();
            enteredMethodId = validateIntUserInput(inputForMethod);
        }
        switch (enteredMethodId) {
            case 1:
                createHotelRoomsViaConsole(hotel, sc);
                inputForRoleMethod(sc, hotel);
            case 2:
                try {
                    createHotelRoomsViaCSV(hotel);
                } catch (FileNotFoundException e) {
                    LOGGER.info(resource.getString("message.file.not.found"));
                } catch (Exception e) {
                    LOGGER.info(resource.getString("message.file.error"));
                    e.getStackTrace();
                }

                inputForRoleMethod(sc, hotel);
        }

    }

    private void createHotelRoomsViaCSV(final Hotel hotel) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("src/main/resources/initialData/initialData.csv"));

        while (sc.hasNextLine()) {
            final String line = sc.nextLine();
            final String[] fields = line.split(",");
            int numberOfRooms = Integer.parseInt(fields[2]);


            if (numberOfRooms > 0) {
                for (int i = 1; i <= numberOfRooms; i++) {
                    if (fields[1].equals(LuxurePackage.ORDINARY.toString())) {
                        hotel.addRoom(hotelCreationService.createOrdinaryRoom(RoomType.valueOf(fields[0]), hotelCreationService.getRoomNumberGenerator()));
                    } else if (fields[1].equals(LuxurePackage.DELUX.toString())) {
                        hotel.addRoom(hotelCreationService.createDeluxeRoom(RoomType.valueOf(fields[0]), hotelCreationService.getRoomNumberGenerator()));
                    } else if (fields[1].equals(LuxurePackage.VIP.toString())) {
                        hotel.addRoom(hotelCreationService.createVipApartment(hotelCreationService.getRoomNumberGenerator()));
                    }

                }
            }
        }

    }

    private void createHotelRoomsViaConsole(final Hotel hotel, final Scanner sc) {
        LOGGER.info(resource.getString("message.enter.number.ordinary.single.room") + lineSeparator);
        int numberOfRooms = validateNextInt(sc);
        if (numberOfRooms >= 1) {
            for (int i = 1; i <= numberOfRooms; i++) {
                hotel.addRoom(hotelCreationService.createOrdinaryRoom(RoomType.SINGLE_ROOM, hotelCreationService.getRoomNumberGenerator()));
            }
        }
        LOGGER.info(resource.getString("message.enter.number.ordinary.double.room"));
        numberOfRooms = validateNextInt(sc);
        if (numberOfRooms >= 1) {
            for (int i = 1; i <= numberOfRooms; i++) {
                hotel.addRoom(hotelCreationService.createOrdinaryRoom(RoomType.DOUBLE_ROOM, hotelCreationService.getRoomNumberGenerator()));
            }
        }
        LOGGER.info(resource.getString("message.enter.number.deluxe.single.room"));
        numberOfRooms = validateNextInt(sc);
        if (numberOfRooms >= 1) {
            for (int i = 1; i <= numberOfRooms; i++) {
                hotel.addRoom(hotelCreationService.createDeluxeRoom(RoomType.SINGLE_ROOM, hotelCreationService.getRoomNumberGenerator()));
            }
        }
        LOGGER.info(resource.getString("message.enter.number.deluxe.double.room"));
        numberOfRooms = validateNextInt(sc);
        if (numberOfRooms >= 1) {
            for (int i = 1; i <= numberOfRooms; i++) {
                hotel.addRoom(hotelCreationService.createDeluxeRoom(RoomType.DOUBLE_ROOM, hotelCreationService.getRoomNumberGenerator()));
            }
        }
        LOGGER.info(resource.getString("message.enter.number.vip.apartment.room"));
        numberOfRooms = validateNextInt(sc);
        if (numberOfRooms >= 1) {
            for (int i = 1; i <= numberOfRooms; i++) {
                hotel.addRoom(hotelCreationService.createVipApartment(hotelCreationService.getRoomNumberGenerator()));
            }
        }
        LOGGER.info(resource.getString("message.creation.done"));
    }

    private int validateNextInt(final Scanner sc) {
        int result = 0;
        while (!sc.hasNextInt()) {
            LOGGER.info(resource.getString("message.enter.num"));
            sc.nextLine();
        }
        result = sc.nextInt();
        return result;
    }

    private int validateIntUserInput(final String input) {
        int result;
        try {
            result = Integer.parseInt(input);

        } catch (NumberFormatException e) {
            result = -1;
        }
        return result;
    }
}



