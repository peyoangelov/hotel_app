package com.scalefocus.services.impl;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.scalefocus.entities.Hotel;
import com.scalefocus.entities.Reservation;
import com.scalefocus.entities.Room;
import com.scalefocus.services.ReportGeneratorInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.FileHandler;


public class ReportGenerator implements ReportGeneratorInterface {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportGenerator.class);
    private static final String lineSeparator = System.getProperty("line.separator");
    HotelAdministrationService hotelAdministrationService = new HotelAdministrationService();
    FileHandler fh;


    @Override
    public void generateReportForTakenRoomsInJSON(final Hotel hotel) {
        try {
            JsonFactory jsonFactory = new JsonFactory();
            jsonFactory.enable(JsonParser.Feature.ALLOW_COMMENTS);
            jsonFactory.disable(JsonParser.Feature.ALLOW_SINGLE_QUOTES);
            FileOutputStream fileOutputStream = new FileOutputStream(new File("src\\main\\resources\\reports\\hotel_report_busyRooms.json"));
            JsonGenerator jsonGenerator = jsonFactory.createGenerator(fileOutputStream);
            jsonGenerator.writeStartObject();
            jsonGenerator.writeFieldName("busyRooms");

            jsonGenerator.writeStartArray();
            for (Room r : hotel.getAllRooms()) {
                for (Reservation res : r.getReservationsList()) {
                    hotelAdministrationService.calculateReservationPrice(res, r);
                    jsonGenerator.writeStartObject();
                    jsonGenerator.writeStringField("guestName", res.getGuestName());
                    jsonGenerator.writeNumberField("roomNumber", r.getEntityId());
                    jsonGenerator.writeStringField("arrivalDate", res.getFormattedArrivalDate());
                    jsonGenerator.writeStringField("leaveDate", res.getFormattedLeaveDate());
                    jsonGenerator.writeNumberField("price", r.getPrice());
                    jsonGenerator.writeStringField("roomType", r.getRoomType().getName());
                    jsonGenerator.writeStringField("packageType", r.getLuxurePackage().getName());
                    jsonGenerator.writeEndObject();
                }
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
            jsonGenerator.close();
        } catch (SecurityException e) {
            e.printStackTrace();
            e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
            e.getMessage();
        }
    }

    @Override
    public void generateReportForFreeRoomsInJSON(Hotel hotel, Date arrivalDate, Date leaveDate) {
        try {
            JsonFactory jsonFactory = new JsonFactory();
            jsonFactory.enable(JsonParser.Feature.ALLOW_COMMENTS);
            jsonFactory.disable(JsonParser.Feature.ALLOW_SINGLE_QUOTES);
            FileOutputStream fileOutputStream = new FileOutputStream(new File("src\\main\\resources\\reports\\hotel_report_freeRooms.json"));
            JsonGenerator jsonGenerator = jsonFactory.createGenerator(fileOutputStream);
            jsonGenerator.writeStartObject();
            jsonGenerator.writeFieldName("freeRooms");
            jsonGenerator.writeStartArray();
            for (Room r : hotel.getAllRooms()) {
                if (r.isRoomFreeForGivenPeriod(arrivalDate, leaveDate)) {
                    jsonGenerator.writeStartObject();
                    jsonGenerator.writeNumberField("roomNumber", r.getEntityId());
                    jsonGenerator.writeNumberField("price", r.getPrice());
                    jsonGenerator.writeStringField("roomType", r.getRoomType().getName());
                    jsonGenerator.writeStringField("packageType", r.getLuxurePackage().getName());
                    jsonGenerator.writeEndObject();
                }
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
            jsonGenerator.close();
        } catch (SecurityException e) {
            e.printStackTrace();
            e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
            e.getMessage();
        }
    }

    @Override
    public void generateReportForFreeRoomsInHTML(Hotel hotel, Date arrivalDate, Date leaveDate) {
        try {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String currentTime = (sdf.format(cal.getTime()));
            FileWriter writer = new FileWriter("src\\main\\resources\\reports\\hotel_report_freeRooms.html", false);

            writer.write("<h1>List of Free rooms</h1>&nbsp;" + "Current Time: <span>" + currentTime + "</span></br>");
            for (Room r : hotel.getAllRooms()) {
                if (r.isRoomFreeForGivenPeriod(arrivalDate, leaveDate)) {
                    writer.write("</br>");
                    writer.write("<div>");
                    writer.write("<span>&nbsp; Room number: " + r.getEntityId() + "</span>&nbsp;&nbsp;");
                    writer.write("<span>&nbsp; Room price: " + r.getPrice() + "</span>&nbsp; &nbsp;");
                    writer.write("<span>&nbsp; Room type: " + r.getRoomType().getName() + "</span>&nbsp;&nbsp;");
                    writer.write("<span>&nbsp; Room package: " + r.getLuxurePackage().getName() + "</span>&nbsp;&nbsp;");
                    writer.write("</div>");
                }
            }
            writer.close();
        } catch (SecurityException e) {
            e.printStackTrace();
            e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
            e.getMessage();
        }
    }


}

