package com.scalefocus.services;

import com.scalefocus.entities.Hotel;

import java.util.Date;

public interface ReportGeneratorInterface {
    void generateReportForTakenRoomsInJSON(final Hotel hotel);
    void generateReportForFreeRoomsInJSON(final Hotel hotel, final Date arrivalDate, final Date leaveDate);
    void generateReportForFreeRoomsInHTML(final Hotel hotel, final Date arrivalDate, final Date leaveDate);

}
