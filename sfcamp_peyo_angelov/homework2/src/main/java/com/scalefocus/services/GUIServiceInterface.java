package com.scalefocus.services;

import com.scalefocus.entities.Hotel;

import java.util.Scanner;
import java.util.Timer;

public interface GUIServiceInterface {

    void makeReportEvery30s(final Hotel hotel, final HotelAdministrationServiceInterface hotelAdministrationService, final Timer timer);

    void inputForRoleMethod(Scanner sc, Hotel hotel);
    void createHotelRooms(final Hotel hotel,  final Scanner sc);
}
