package com.scalefocus.services;

import com.scalefocus.entities.Hotel;

public interface HotelAdministrationServiceInterface {
    void collectInformationAboutOccupiedRooms(Hotel hotel);
}
