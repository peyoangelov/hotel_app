package com.scalefocus.services.impl;

import com.scalefocus.entities.Hotel;
import com.scalefocus.entities.Room;
import com.scalefocus.enums.LuxurePackage;
import com.scalefocus.enums.RoomType;
import com.scalefocus.exceptions.InvalidPackageTypeException;
import com.scalefocus.services.HotelCreationServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

import static com.scalefocus.constants.HotelConstants.RoomConsts.*;
import static com.scalefocus.enums.RoomType.DOUBLE_ROOM;
import static com.scalefocus.enums.RoomType.SINGLE_ROOM;


public class HotelCreationService implements HotelCreationServiceInterface {

    static int roomNumberGenerator = 1;
    private static final Logger LOGGER = LoggerFactory.getLogger(HotelCreationService.class);
    private static final String lineSeparator = System.getProperty("line.separator");

    /**
     * Creating ordinary room( single or double, depends on packageId 1 or 2 )
     */
    @Override
    public Room createOrdinaryRoom(final RoomType roomType, int roomNumber) {

        int packageId = roomType.equals(SINGLE_ROOM) ? ORDINARY_SINGLE_ROOM_CODE : ORDINARY_DOUBLE_ROOM_CODE;
        BigDecimal roomPrice = roomType.equals(SINGLE_ROOM) ? ORDINARY_SINGLE_ROOM_PRICE : ORDINARY_DOUBLE_ROOM_PRICE;
        try {
            return new Room(roomType, LuxurePackage.ORDINARY, packageId, roomNumber, roomPrice, false, false, false,
                    true, false, false, false);
        } catch (final InvalidPackageTypeException e) {
            LOGGER.info(e.getMessage() + " for room number " + roomNumber);
            return null;
        }
    }

    /**
     * Creating deluxe room ( single or double, depends on packageId 3 or 4 )
     */
    @Override
    public Room createDeluxeRoom(final RoomType roomType, int roomNumber) {

        int packageId = roomType.equals(SINGLE_ROOM) ? DELUX_SINGLE_ROOM_CODE : DELUX_DOUBLE_ROOM_CODE;
        BigDecimal roomPrice = roomType.equals(SINGLE_ROOM) ? DELUX_SINGLE_ROOM_PRICE : DELUX_DOUBLE_ROOM_PRICE;
        try {
            return new Room(roomType, LuxurePackage.DELUX, packageId, roomNumber, roomPrice, true, true, true,
                    true, false, false, false);
        } catch (final InvalidPackageTypeException e) {
            LOGGER.info(e.getMessage() + " for room number " + roomNumber);
            return null;
        }
    }

    /**
     * Creating vip apartment
     */
    @Override
    public Room createVipApartment(int roomNumber) {
        try {
            return new Room(RoomType.APARTMENT, LuxurePackage.VIP, VIP_APARTMENT_CODE, roomNumber, VIP_APARTMENT_PRICE, true, true, true,
                    true, true, true, true);
        } catch (final InvalidPackageTypeException e) {
            LOGGER.info(e.getMessage() + " for room number " + roomNumber);
            return null;
        }
    }

    /**
     * increments room's number every time when the new room is created
     */
    @Override
    public int getRoomNumberGenerator() {
        return roomNumberGenerator++;
    }

    /**
     * Instantiating Hotel class with variable hotel
     * Filling the hotel with rooms
     */
    @Override
    public Hotel fillTheHotelWithRooms() {

        Hotel hotel = Hotel.getInstance();

        for (int i = 1; i <= 5; i++) {
            hotel.addRoom(createOrdinaryRoom(SINGLE_ROOM, getRoomNumberGenerator()));
            hotel.addRoom(createOrdinaryRoom(DOUBLE_ROOM, getRoomNumberGenerator()));
            hotel.addRoom(createDeluxeRoom(SINGLE_ROOM, getRoomNumberGenerator()));
            hotel.addRoom(createDeluxeRoom(DOUBLE_ROOM, getRoomNumberGenerator()));
            hotel.addRoom(createVipApartment(getRoomNumberGenerator()));
        }
        return hotel;
    }

}
