package com.scalefocus.services.impl;


import com.scalefocus.entities.Hotel;
import com.scalefocus.entities.Reservation;
import com.scalefocus.entities.Room;
import com.scalefocus.services.HotelAdministrationServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class HotelAdministrationService implements HotelAdministrationServiceInterface {

    private final Locale locale = new Locale("en", "EN");
    private final ResourceBundle resource = ResourceBundle.getBundle("locale", locale);
    private static final Logger LOGGER = LoggerFactory.getLogger(HotelAdministrationService.class);
    private static final String lineSeparator = System.getProperty("line.separator");

    /**
     * Auto report on every 30s for occupied rooms
     *
     * @param hotel
     */
    @Override
    public void collectInformationAboutOccupiedRooms(final Hotel hotel) {
        int numberOfReservations = 0;

        for (Room r : hotel.getAllRooms()) {
            for (Reservation res : r.getReservationsList()) {
                calculateReservationPrice(res, r);
                LOGGER.info(MessageFormat.format(resource.getString("message.auto.report.occupied.rooms"), lineSeparator, r.getEntityId(), res.getFormattedArrivalDate(), res.getFormattedLeaveDate(), res.getPrice()));
                numberOfReservations++;
            }
        }
        LOGGER.info(MessageFormat.format(resource.getString("message.auto.report.occupied.rooms.summary"), numberOfReservations) + lineSeparator);
    }

    /**
     * Calculate the price of the reservation
     *
     * @param reservation
     * @param room
     * @return
     */
    public void calculateReservationPrice(final Reservation reservation, final Room room) {
        final Date arrival = reservation.getArrivalDate();
        final Date leaving = reservation.getLeaveDate();
        int daysBetween = daysBetween(leaving, arrival);
        final BigDecimal roomPrice = room.getPrice();
        final BigDecimal reservationPrice = roomPrice.multiply(new BigDecimal(daysBetween));
        reservation.setPrice(reservationPrice);
    }

    /**
     * Finding how much days the guest have stayed
     *
     * @param d1
     * @param d2
     * @return
     */
    private int daysBetween(final Date d1, final Date d2) {
        return (int) ((d1.getTime() - d2.getTime()) / (1000 * 60 * 60 * 24));
    }
}
