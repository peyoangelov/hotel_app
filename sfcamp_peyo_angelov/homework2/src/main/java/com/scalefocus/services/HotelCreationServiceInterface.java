package com.scalefocus.services;

import com.scalefocus.entities.Hotel;
import com.scalefocus.entities.Room;
import com.scalefocus.enums.RoomType;

public interface HotelCreationServiceInterface {

    Room createOrdinaryRoom(RoomType roomType, int roomNumber);

    Room createDeluxeRoom(RoomType roomType, int roomNumber);

    Room createVipApartment(int roomNumber);

    int getRoomNumberGenerator();

    Hotel fillTheHotelWithRooms();


}
