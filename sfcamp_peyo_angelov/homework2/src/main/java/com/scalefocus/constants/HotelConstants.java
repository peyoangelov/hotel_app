package com.scalefocus.constants;

import java.math.BigDecimal;

/**
 * Global interface for all constants
 */
public interface HotelConstants {

    interface RoomConsts {
        int ORDINARY_SINGLE_ROOM_CODE = 1;
        int ORDINARY_DOUBLE_ROOM_CODE = 2;
        int DELUX_SINGLE_ROOM_CODE = 3;
        int DELUX_DOUBLE_ROOM_CODE = 4;
        int VIP_APARTMENT_CODE = 5;
        BigDecimal ORDINARY_SINGLE_ROOM_PRICE = new BigDecimal(50);
        BigDecimal ORDINARY_DOUBLE_ROOM_PRICE = new BigDecimal(80);
        BigDecimal DELUX_SINGLE_ROOM_PRICE = new BigDecimal(75);
        BigDecimal DELUX_DOUBLE_ROOM_PRICE = new BigDecimal(100);
        BigDecimal VIP_APARTMENT_PRICE = new BigDecimal(200);
    }

    interface Date{
        String DATE_FORMAT = "dd/MM/yyyy";
        String DATE_REGEX="^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$";
    }

    interface Timer{
        int TIMER_INTERVAL = 30000;
    }
    interface Methods{
        int MAX_VALUE_FOR_METHOD_FOR_ADMIN = 6;
        int MIN_VALUE_FOR_METHOD_FOR_ADMIN = 1;
        int MAX_VALUE_FOR_METHOD_FOR_GUEST = 4;
        int MIN_VALUE_FOR_METHOD_FOR_GUEST = 1;
        int MAX_VALUE_FOR_METHOD_FOR_FRONT_DESC = 4;
        int MIN_VALUE_FOR_METHOD_FOR_FRONT_DESC = 1;
    }

}
