package com.scalefocus.demo;


import com.scalefocus.entities.Hotel;
import com.scalefocus.services.GUIServiceInterface;
import com.scalefocus.services.HotelAdministrationServiceInterface;
import com.scalefocus.services.HotelCreationServiceInterface;
import com.scalefocus.services.impl.GUIService;
import com.scalefocus.services.impl.HotelAdministrationService;
import com.scalefocus.services.impl.HotelCreationService;

import java.util.Scanner;
import java.util.Timer;

public class Demo {
    public static void main(String[] args) {
        final Scanner sc = new Scanner(System.in);
        final Timer timer = new Timer();
        final HotelAdministrationServiceInterface hotelAdministrationService = new HotelAdministrationService();
        final HotelCreationServiceInterface hotelService = new HotelCreationService();
        final GUIServiceInterface guiService = new GUIService();
        final Hotel hotel = Hotel.getInstance();


        guiService.makeReportEvery30s(hotel, hotelAdministrationService, timer);
        guiService.createHotelRooms(hotel,sc);
        sc.close();
    }
}

