# Scale Camp


![N|Solid](http://iteventz.bg/images/upload/8ea09dd1e475560c17ffd16f737ce7bc.jpg)

# Peyo Angelov's repository.
## Module 1:
Scale camp modules:
  - Indoruction to Java
  - OOP in Java
  - Unit Test
  - Exceptions and exceptions hangling.
  - Data structute 
  - Files Input/Output streams.
  - Design patterns 
  - Logging and debugging
  - High quality code
  
## Module 2:
  - RDBMS
  - SQL basic
  - SQL advance
  - JDBC and hibernate

## Module 3:
  - GIT advanced
  - THREADS
  - PERFORMANCE
  - OS
  - Build tools
  - Spring
  - Spring Data 
  - Spring MVC
  - Spring Boot
  - Spring REST
  - AGILE
  
## FINAL MODULE
  - WORK ON A REAL PROJECT