package car;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

public class Car {

    private CarModel model;
    private CarColor color;
    private Date dateOfFirstRegistration;
    private BigDecimal price;


    public Car(CarModel model, CarColor color, Date dateOfFirstRegistration, BigDecimal price) {
        this.setModel(model);
        this.setColor(color);
        this.setDateOfFirstRegistration(dateOfFirstRegistration);
        this.setPrice(price);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return getModel() == car.getModel() &&
                getColor() == car.getColor() &&
                Objects.equals(getDateOfFirstRegistration(), car.getDateOfFirstRegistration()) &&
                Objects.equals(getPrice(), car.getPrice());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getModel(), getColor(), getDateOfFirstRegistration(), getPrice());
    }

    @Override
    public String toString() {
        return "Car{" +
                "model=" + model +
                ", color=" + color +
                ", dateOfFirstRegistration=" + dateOfFirstRegistration +
                ", price=" + price +
                '}';
    }

    public CarModel getModel() {
        return model;
    }

    public void setModel(CarModel model) {
        this.model = model;
    }

    public CarColor getColor() {
        return color;
    }

    public void setColor(CarColor color) {
        this.color = color;
    }

    public Date getDateOfFirstRegistration() {
        return dateOfFirstRegistration;
    }

    public void setDateOfFirstRegistration(Date dateOfFirstRegistration) {
        this.dateOfFirstRegistration = dateOfFirstRegistration;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
