import car.Car;
import car.CarColor;
import car.CarModel;

import java.math.BigDecimal;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        /**
         * Sorting a map
         */
        System.out.println("---------------");
        System.out.println("Testing sorting of the map by value ");
        MapSorterByValue mapSorterByValue = new MapSorterByValue();
        final Map<String, Integer> sortedByCount = mapSorterByValue.sortByValue();
        System.out.println("Sorted by horse power" + sortedByCount);
        System.out.println("---------------");


        /**
         *  Implementing Array List
         */
        System.out.println(" ");
        System.out.println("---------------");
        System.out.println("Testing my ARRAY LIST ");
        MyArrayList mal = new MyArrayList();
        mal.add(new Integer(2));
        mal.add(new Integer(5));
        mal.add(new Integer(1));
        mal.add(new Integer(23));
        mal.add(new Integer(14));
        for (int i = 0; i < mal.size(); i++) {
            System.out.print(mal.get(i) + " ");
        }
        mal.add(new Integer(29));
        System.out.println("Element at Index 5:" + mal.get(5));
        System.out.println("List size: " + mal.size());
        System.out.println("Removing element at index 2: " + mal.remove(2));
        for (int i = 0; i < mal.size(); i++) {
            System.out.print(mal.get(i) + " ");
        }
        System.out.println(" ");
        System.out.println("---------------");

        /**
         * STACK TASK
         */
        System.out.println(" ");
        System.out.println("---------------");
        System.out.println("Sorting Stack");
        SorterStack sorterStack = new SorterStack();

        Stack<Integer> input = new Stack<>();
        input.add(34);
        input.add(3);
        input.add(31);
        input.add(98);
        input.add(92);
        input.add(23);

        // This is the temporary stack
        Stack<Integer> tmpStack = sorterStack.sortstack(input);
        System.out.println("Sorted numbers are:");

        while (!tmpStack.empty()) {
            System.out.print(tmpStack.pop() + " ");
        }

        /**
         * CAR TASK
         */
        System.out.println(" ");
        System.out.println("---------------");

        System.out.println("CAR OBJECTS");
        final Set<Car> cars = new HashSet<>();
        final Car car1 = new Car(CarModel.AUDI, CarColor.BLACK, new Date(12 / 12 / 2019), new BigDecimal(1));
        final Car car2 = new Car(CarModel.AUDI, CarColor.BLACK, new Date(12 / 12 / 2019), new BigDecimal(1));
        final Car car3 = new Car(CarModel.MERCEDES, CarColor.BLUE, new Date(12 / 12 / 2019), new BigDecimal(100));
        final Car car4 = new Car(CarModel.MERCEDES, CarColor.BLACK, new Date(12 / 12 / 2019), new BigDecimal(100));
        final Car car5 = new Car(CarModel.BMW, CarColor.BLACK, new Date(12 / 12 / 2019), new BigDecimal(10000));
        final Car car6 = new Car(CarModel.BMW, CarColor.BLACK, new Date(12 / 12 / 2019), new BigDecimal(10000));
        cars.add(car1);
        cars.add(car2);
        cars.add(car3);
        cars.add(car4);
        cars.add(car5);
        cars.add(car6);
        for (Car car : cars) {
            System.out.println(car.toString());
        }


    }


}
