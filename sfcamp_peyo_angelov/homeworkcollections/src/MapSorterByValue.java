import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class MapSorterByValue {
    public static Map<String, Integer> sortByValue() {
        final Map<String, Integer> carsWithHorsePower = new HashMap<>();
        carsWithHorsePower.put("Audi", 100);
        carsWithHorsePower.put("BMW", 200);
        carsWithHorsePower.put("Lada", 50);
        carsWithHorsePower.put("Fiat", 70);
        carsWithHorsePower.put("Porsche", 200);

        return carsWithHorsePower.entrySet()
                .stream()
                .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

}
